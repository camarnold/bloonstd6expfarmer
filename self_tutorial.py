from pyautogui import press, click, position, moveRel, mouseDown, mouseUp
from time import sleep


'''
This script assumes that we start at the Main Menu.

This will play Meadows on Impoppable infinitely until
the Failsafe is used (move mouse to top left of the screen).
'''


print('In order to exit, please move the mouse to the top left of the screen manually.')
sleep(3)

##############################
# Main Menu
##############################

# Select Obyn

click(600, 960)
sleep(1)
click(725, 977)
sleep(1)
click(635, 665)
sleep(0.7)
press('esc')
sleep(0.7)


##############################
# Main Loop
##############################
while True:
    # Get into game
    click(837, 961)
    sleep(0.7)
    click(1330, 1000)
    sleep(0.7)
    click(1330, 1000)
    sleep(0.7)
    click(531, 279)
    sleep(0.7)
    click(650, 440)
    sleep(0.7)
    click(1290, 460)
    sleep(4)


    click(957, 759); sleep(0.8)


    # Obyn
    x, y = position()
    press('u'); sleep(0.3); moveRel(568-x, 490-y, duration=0.3)
    mouseDown(); sleep(0.1); mouseUp()


    # Monkey Village
    sleep(0.2)
    x, y = position()
    press('k'); sleep(0.3); moveRel(839-x, 302-y, duration=0.3)
    mouseDown(); sleep(0.1); mouseUp(); sleep(0.3)
    click(839, 302); sleep(0.3); press('/'); sleep(0.2); press('/')


    # Super Monkey
    sleep(0.2)
    x, y = position()
    press('s'); sleep(0.3); moveRel(747-x, 429-y, duration=0.3)
    mouseDown(); sleep(0.1); mouseUp(); sleep(0.3)
    click(747, 429); sleep(0.3); press('/'); sleep(0.2); press('/')
    sleep(0.2); press('/'); sleep(0.2); press(','); sleep(0.2); press(',')


    # Dart Monkey
    sleep(0.2)
    x, y = position()
    press('q'); sleep(0.3); moveRel(842-x, 445-y, duration=0.3)
    mouseDown(); sleep(0.1); mouseUp(); sleep(0.3)
    click(842, 445); sleep(0.3); press('/'); sleep(0.2); press('/')
    sleep(0.2); press('/'); sleep(0.2); press('.'); sleep(0.2); press('.')


    # Super Monkey but weaker
    sleep(0.2)
    x, y = position()
    press('s'); sleep(0.3); moveRel(685-x, 334-y, duration=0.3)
    mouseDown(); sleep(0.1); mouseUp(); sleep(0.3)
    click(685, 334); sleep(0.3); press('.')


    # Sell the Monkey Village, Upgrade the Dart Monkey one last time, and buy ninja monkey
    sleep(0.2)
    click(839, 302); sleep(0.3); press('backspace'); sleep(0.2)
    click(842, 445); sleep(0.3); press('/')

    sleep(0.2)
    press('d'); sleep(0.3); moveRel(551-x, 623-y, duration=0.3)
    mouseDown(); sleep(0.1); mouseUp(); sleep(0.3)
    click(551, 623); sleep(0.3); press(',')


    # Start
    sleep(0.2); press('space'); sleep(0.8); press('space')


    # Go to Main Menu
    sleep(0.2)
    click(960, 900); sleep(0.8); click(800,850)
    sleep(3)
