import pyautogui
import time

print('In order to exit, please move the mouse to the top left of the screen manually.')
time.sleep(5)
distance = 800
try:
    pyautogui.click(300,300,button='left')
    while distance > 0:
        pyautogui.dragRel(distance, 0, duration=distance/3000)   # move right
        distance = distance - 4
        pyautogui.dragRel(0, distance, duration=distance/3000)   # move down
        pyautogui.dragRel(-distance, 0, duration=distance/3000)  # move
        distance = distance - 4
        pyautogui.dragRel(0, -distance, duration=distance/3000)  # move up
except KeyboardInterrupt:
    pass
