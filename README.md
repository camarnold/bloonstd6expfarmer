# BloonsTD6ExpFarmer

Run main.py, switch to a freshly opened game of BloonsTD6 (Title Screen) that's in fullscreen and (assuming you have a 1080p display) this script will play for you for as long as you wish gaining experience while you go off and do other fun things!

If you have a different resolution, just scale each value to your specific resolution and it *should* work.
